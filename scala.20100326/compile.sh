#!/bin/sh

[ ! -d classes ] && mkdir -p classes
scalac -d classes "$@"